# JSP Environment Setup
---

### Java:

* Java 6.0 or above - http://www.oracle.com/technetwork/java/javase/downloads/index.html

### Apache Tomcat:

* Download Apache: https://tomcat.apache.org/download-70.cgi
* Apache Tomcat 7(Documentation): http://tomcat.apache.org/tomcat-7.0-doc/setup.html
* Installation instruction(Linux) : http://www.linux-sxs.org/internet_serving/c140.html
* Tomcat Installation video: https://www.youtube.com/watch?v=6-yaGFQK9Ng
* Tomcat version comparision : http://tomcat.apache.org/whichversion.html
																																																																																																																																																																																																																																																																																																																																																																																																							
### Eclipse:

* Eclipse IDE for java EE Devs: http://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/marsr (OR)
* Add the plugin to existing Eclipse: http://stackoverflow.com/questions/6805880/how-to-install-tomcat-plugin-in-eclipse
* Configuring Tomcat plugin in eclipse: https://youtu.be/b42CJ0r-1to?list=PLE0F6C1917A427E96

### Useful References:
* http://www.tutorialspoint.com/jsp/jsp_environment_setup.htm